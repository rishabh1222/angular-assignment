import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private resourceUrl = `${environment.apiUrl}/api`;
  constructor(private http: HttpClient) { }
  
  getUserList(page: number, per_page: number) {
    return this.http.get<any>(`${this.resourceUrl}/users?page=${page}?per_page=${per_page}`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }

  deleteUser(id: number) {
    return this.http.delete<any>(`${this.resourceUrl}/users/${id}`, { observe: 'response' });
  }
  getUserDetails(id) {
    return this.http.get<any>(`${this.resourceUrl}/users/${id}`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  updateUserDetails(id, params) {
    return this.http.put<any>(`${this.resourceUrl}/users${id}`, params)
      .pipe(map(res => {
        return res;
      }));
  }
}
