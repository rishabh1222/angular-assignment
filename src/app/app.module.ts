import { BrowserModule } from '@angular/platform-browser';
import { NgModule, } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserListComponent } from './components/user-list/user-list.component';
import {
  MatNativeDateModule, MatDatepickerModule,
  MatIconModule, MatButtonModule, MatCheckboxModule,
  MatToolbarModule, MatCardModule, MatFormFieldModule,
  MatInputModule, MatRadioModule, MatListModule,
  MatDialogModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UsersComponent } from './pages/users/users.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import {UserDetailsComponent} from './components/user-details/user-details.component';
import { UserDetailsEditComponent } from './pages/user-details-edit/user-details-edit.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserListComponent,
    HeaderComponent,
    UsersComponent,
    ConfirmationDialogComponent,
    UserDetailsComponent,
    UserDetailsEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatNativeDateModule, MatDatepickerModule, MatIconModule,
    MatButtonModule, MatCheckboxModule, MatToolbarModule,
    MatCardModule, MatFormFieldModule, MatInputModule,
    MatRadioModule, MatListModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    Ng2SearchPipeModule
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
