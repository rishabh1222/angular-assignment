import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service'
import { first } from 'rxjs/operators';
import { User } from '../../models/user'
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public userList: User[];
  page = 1
  per_page = 10
  public loading:boolean = false
  constructor(private UserService: UserService, 
    private dialog: MatDialog,
    private router:Router) { }

  ngOnInit() {
    this.getUserList()
  }

  getUserList() {
    this.loading = true
    this.UserService.getUserList(this.page, this.per_page)
      .pipe(first())
      .subscribe(
        data => {
          this.userList = data.data
          this.loading = false
          console.log(data)
        },
        error => {
          console.log(error)
          this.loading = false;
        });
  }

  deleteUser(id,index) {
    console.log(index)
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: "Are you sure you want to delete this user?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.UserService.deleteUser(id)
        .pipe(first())
        .subscribe(
          data => {
            this.userList.splice(index,1)
          },
          error => {
            console.log(error)
            // this.loading = false;
          });
      }
    });
  }

  showUserDetails(id){
    this.router.navigate(['/user-details'], { queryParams: { user: id } });
   }



}
