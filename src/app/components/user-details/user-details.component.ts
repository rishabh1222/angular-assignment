import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../services/user.service'
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @Input() userId: number
  public loading: boolean;
  public userDetails: any;
  public UserDetailsForm: FormGroup;
  public editMode: boolean;
  public error: string;
  constructor(private UserService: UserService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getUserList()
  }
  enableInputs(value: boolean) {
    this.editMode = value;
    this.error = null
    if (value) {
      this.UserDetailsForm.controls['first_name'].enable();
      this.UserDetailsForm.controls['last_name'].enable();
      this.UserDetailsForm.controls['email'].enable();
    } else {
      this.UserDetailsForm.controls['first_name'].disable();
      this.UserDetailsForm.controls['last_name'].disable();
      this.UserDetailsForm.controls['email'].disable();
      this.setForm()
    }
  }
  setForm() {
    console.log(this.userDetails)
    this.UserDetailsForm = this.formBuilder.group({
      email: [{ value: this.userDetails.email, disabled: true }, Validators.required],
      first_name: [{ value: this.userDetails.first_name, disabled: true }, Validators.required],
      last_name: [{ value: this.userDetails.last_name, disabled: true }, Validators.required],
      avatar: [this.userDetails.avatar, Validators.required]
    });
  }
  getUserList() {
    this.UserService.getUserDetails(this.userId)
      .pipe(first())
      .subscribe(
        res => {
          this.userDetails = res.data
          this.setForm()
          this.loading = false
          console.log(res)
        },
        error => {
          console.log(error)
    
        });

  }
  saveChanges(){ 
    if(this.UserDetailsForm.invalid){
      this.error = 'All fields are mandatory'
      return;
    }
    this.loading = true;
    this.UserService.updateUserDetails(this.userId,this.UserDetailsForm.value)
    .pipe(first())
    .subscribe(
      res => {
        this.userDetails = res
        console.log(this.userDetails,res)
        this.enableInputs(false)
        this.loading = false
    
      },
      error => {
        this.error =error.error
        this.loading = false;
      });

  }
}
