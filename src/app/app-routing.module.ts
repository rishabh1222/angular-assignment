import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { UsersComponent } from './pages/users/users.component';
import { UserDetailsEditComponent } from './pages/user-details-edit/user-details-edit.component'
const routes: Routes = [
  {
    path: 'users',
    canActivate: [AuthGuard], // here we tell Angular to check the access with our AuthGuard
    component: UsersComponent,
  },
  {
    path: 'user-details',
    canActivate: [AuthGuard], // here we tell Angular to check the access with our AuthGuard
    component: UserDetailsEditComponent,
  },
  { path: 'login', component: LoginComponent },
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
